// ACTIVITY
// 1, 2, 4, 5, 6, 7, 8

class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.gradeAve = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;

    if (grades.length === 4) {
      if (grades.every(grade => grade >= 0 && grade <= 100)) {
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out`);
    return this
  }
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    return this;
  }
  computeAve() {
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    this.gradeAve = sum / 4;
    return this;
  }
  willPass() {
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }

  willPassWithHonors() {
    if (this.passed) {
      if (this.gradeAve >= 90) {
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
}

class Section {
  constructor(name) {
    this.name = name;
    this.students = [];
    this.honorStudents = undefined;
  }

  addStudent(name, email, grades) {
    const student = new Student(name, email, grades);
    this.students.push(student);
    return student;
  }

  countStudents() {
    const totalStudents = this.students.length;
    let totalHonorStudents = 0;
    this.students.forEach(student => {
      if (student.willPassWithHonors()) {
        totalHonorStudents++;
      }
    });
    this.honorStudents = totalHonorStudents;
    return { totalStudents, totalHonorStudents };
  }

  countHonorStudents() {
    let count = 0;
    this.students.forEach(student => {
      if (student.willPass().willPassWithHonors().passedWithHonors) {
        count++;
      }
    })
    this.honorStudents = count;
    return this;
  }

  computeHonorsPercentage() {
    const { totalStudents, totalHonorStudents } = this.countStudents();
    this.honorsPercentage = (totalHonorStudents / totalStudents) * 100;
    return this;
  }
}

class Grade {
  constructor(level) {
    this.level = level;
    this.sections = [];
    this.totalStudents = 0;
    this.totalHonorStudents = 0;
    this.batchAveGrade = undefined;
    this.batchMinGrade = undefined;
    this.batchMaxGrade = undefined;
  }

    // Method for adding the created section to the sections array
  addSection(section) {
    this.sections.push(section);
    return this;
  }

    /*  Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.*/
  countStudents() {
    this.sections.forEach(section => {
      this.totalStudents += section.students.length;
    })
    return this;
  }

    /* Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property. */
  countHonorStudents() {
    this.sections.forEach(section => {
      this.totalHonorStudents += section.honorStudents;
    })
    return this;
  }
    /* Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation. */
  computeBatchAve() {
    let total = 0;
    this.sections.forEach(section =>
      section.students.forEach(student => {
        total += student.gradeAve;
      })
      )
    this.batchAveGrade = total / this.totalStudents;
    return this;
  }

    /* Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section. */
  getBatchMinGrade() {
    let min = 100;
    this.sections.forEach(section =>
      section.students.forEach(student => {
        student.grades.forEach(grade => {
          if (grade < min) {
            min = grade;
          }
        })
      })
      )
    this.batchMinGrade = min;
    return this;
  }

    /* Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section. */
  getBatchMaxGrade() {
    let max = 0;
    this.sections.forEach(section =>
      section.students.forEach(student => {
        student.grades.forEach(grade => {
          if (grade > max) {
            max = grade;
          }
        })
      })
      )
    this.batchMaxGrade = max;
    return this;
  }
}


// console.log(section1A)
// console.log(section1B)
// console.log(section1C)
// console.log(section1D)
// console.log(grade1);









// 3. 

const section1A = new Section('section1A');
const section1B = new Section('section1B');
const section1C = new Section('section1C');
const section1D = new Section('section1D');

section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
section1B.addStudent('Sam', 'sam@mail.com', [75, 80, 77, 72]);
section1B.addStudent('Lisa', 'lisa@mail.com', [93, 91, 94, 95]);
section1B.addStudent('Tom', 'tom@mail.com', [82, 88, 85, 89]);
section1B.addStudent('Sarah', 'sarah@mail.com', [90, 88, 92, 89]);
section1C.addStudent('Emily', 'emily@mail.com', [86, 84, 83, 89]);
section1C.addStudent('Max', 'max@mail.com', [80, 85, 81, 79]);
section1C.addStudent('Chris', 'chris@mail.com', [94, 92, 95, 93]);
section1C.addStudent('Anna', 'anna@mail.com', [87, 88, 89, 90]);
section1D.addStudent('Jenny', 'jenny@mail.com', [85, 81, 79, 82]);
section1D.addStudent('Kevin', 'kevin@mail.com', [91, 92, 90, 89]);
section1D.addStudent('Tim', 'tim@mail.com', [79, 80, 75, 82]);
section1D.addStudent('Sophie', 'sophie@mail.com', [94, 92, 93, 95]);


const grade1 = new Grade(1);
// grade1.addSection(section1A);

grade1.addSection(section1A);
grade1.addSection(section1B);
grade1.addSection(section1C);
grade1.addSection(section1D);

grade1.countStudents()

section1A.countHonorStudents().computeHonorsPercentage()
section1B.countHonorStudents().computeHonorsPercentage()
section1C.countHonorStudents().computeHonorsPercentage()
section1D.countHonorStudents().computeHonorsPercentage()
grade1.countHonorStudents()

grade1.computeBatchAve()

grade1.getBatchMinGrade()

grade1.getBatchMaxGrade()